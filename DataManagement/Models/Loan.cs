﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataManagement.Models
{
    public class Loan
    {
        public long Id { get; set; }
        public float Debt { get; set; }
        public User Borrower { get; set; }
        public User Lender { get; set; }
        public bool IsRepaid { get; set; }
    }
}
