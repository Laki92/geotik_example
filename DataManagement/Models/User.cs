﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataManagement.Models
{
    public class User
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public float AccountBalance { get; set; }
    }
}
