﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataManagement.Models;
using Geotik_example.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Geotik_example.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoansController : ControllerBase
    {
        private ApplicationDbContext _context;

        public LoansController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpGet]
        public ActionResult<List<UserDto>> GetAllLoanUsers()
        {
            var Users = _context.Loans.Select(x => x.Borrower).Select(x => 
            new UserDto()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                IsBorrower = true
            }).Distinct().ToList();
            var Lenders = _context.Loans.Select(x => x.Lender);
            foreach(var lender in Lenders)
            {
                var user = Users.Where(x => x.Id == lender.Id).FirstOrDefault();
                if(user != null)
                    user.IsLender = true;
                else
                {
                    Users.Add(
                        new UserDto
                        {
                            Id = lender.Id,
                            FirstName = lender.FirstName,
                            LastName = lender.LastName,
                            IsLender = true
                        });
                }
            }
            return Users;
        }

        [HttpGet("{id}")]
        public ActionResult<List<LoanDto>> GetUserLoans(int id)
        {
            var userLoans = _context.Loans.Where(x => x.Borrower.Id == id).Select(x =>
            new LoanDto()
            {
                Id = x.Id,
                Debt = x.Debt,
                Lender = (x.Lender.FirstName + " " + x.Lender.LastName),
                IsRepaid = x.IsRepaid
            }).ToList();

            return userLoans;
        }

        [HttpPost("create-new-user")]
        public IActionResult CreateNewUser([FromBody] UserCreationDto newUser)
        {
            if (newUser == null)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest();

            _context.Users.Add(new User
            {
                FirstName = newUser.FirstName,
                LastName = newUser.LastName,
                AccountBalance = newUser.AccountBalance
            });

            if (_context.SaveChanges() < 1)
                return StatusCode(500, "A problem occured while handling your request.");

            return Created($"api/[controller]", newUser);
        }

        [HttpPost("create-new-loan")]
        public IActionResult CreateNewLoan([FromBody] LoanCreationDto newLoan)
        {
            if (newLoan == null)
                return BadRequest();

            if (!ModelState.IsValid)
                return BadRequest();

            var borrower = _context.Users.Where(x => x.Id == newLoan.BorrowerId).FirstOrDefault();
            var lender = _context.Users.Where(x => x.Id == newLoan.LenderId).FirstOrDefault();

            if (borrower == null || lender == null)
                return BadRequest();

            if (lender.AccountBalance >= newLoan.Debt)
            {
                lender.AccountBalance -= newLoan.Debt;
                borrower.AccountBalance += newLoan.Debt;
                _context.Loans.Add(new Loan
                {
                    Borrower = borrower,
                    Lender = lender,
                    Debt = newLoan.Debt,
                });
            }
            else
                return Forbid();

            if (_context.SaveChanges() < 1)
                return StatusCode(500, "A problem occured while handling your request.");

            return Created($"api/[controller]", newLoan);
        }


        [HttpPut("{id}")]
        public IActionResult PayDebt(int id)
        {
            var loan = _context.Loans.Include(x => x.Borrower).Include(x => x.Lender).Where(x => x.Id == id).SingleOrDefault();

            if (loan == null)
                return BadRequest();

            if (loan.Debt > loan.Borrower.AccountBalance)
                return Forbid();

            loan.Borrower.AccountBalance -= loan.Debt;
            loan.Lender.AccountBalance += loan.Debt;
            loan.IsRepaid = true;

            if (_context.SaveChanges() < 1)
                return StatusCode(500, "A problem occured while handling your request.");

            return NoContent();
        }
    }
}
