﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Geotik_example.Models
{
    public class LoanDto
    {
        public long Id { get; set; }
        public float Debt { get; set; }
        public string Lender { get; set; }
        public bool IsRepaid { get; set; }
    }
}
