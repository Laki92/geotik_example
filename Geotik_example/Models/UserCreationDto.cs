﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Geotik_example.Models
{
    public class UserCreationDto
    {
        [Required(ErrorMessage = "The FirstName field is required."), MaxLength(50)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "The LastName field is required."), MaxLength(50)]
        public string LastName { get; set; }
        [Required(ErrorMessage = "The AccountBalance field is required.")]
        public float AccountBalance { get; set; }
    }
}
