﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Geotik_example.Models
{
    public class LoanCreationDto
    {
        public long LenderId { get; set; }
        public long BorrowerId { get; set; }
        public float Debt { get; set; }
    }
}
